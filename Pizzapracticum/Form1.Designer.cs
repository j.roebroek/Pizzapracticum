﻿namespace Pizzapracticum
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            vorigeKnop = new Button();
            volgendeKnop = new Button();
            panel1 = new Panel();
            label1 = new Label();
            panel3 = new Panel();
            label3 = new Label();
            panel2 = new Panel();
            label2 = new Label();
            panel1.SuspendLayout();
            panel3.SuspendLayout();
            panel2.SuspendLayout();
            SuspendLayout();
            // 
            // vorigeKnop
            // 
            vorigeKnop.Location = new Point(120, 349);
            vorigeKnop.Name = "vorigeKnop";
            vorigeKnop.Size = new Size(94, 29);
            vorigeKnop.TabIndex = 1;
            vorigeKnop.Text = "Vorige";
            vorigeKnop.UseVisualStyleBackColor = true;
            vorigeKnop.Click += vorigeKnop_Click;
            // 
            // volgendeKnop
            // 
            volgendeKnop.Location = new Point(351, 349);
            volgendeKnop.Name = "volgendeKnop";
            volgendeKnop.Size = new Size(94, 29);
            volgendeKnop.TabIndex = 2;
            volgendeKnop.Text = "Volgende";
            volgendeKnop.UseVisualStyleBackColor = true;
            volgendeKnop.Click += volgendeKnop_Click;
            // 
            // panel1
            // 
            panel1.BackColor = SystemColors.Desktop;
            panel1.Controls.Add(label1);
            panel1.Location = new Point(33, 29);
            panel1.Name = "panel1";
            panel1.Size = new Size(501, 300);
            panel1.TabIndex = 3;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(229, 20);
            label1.Name = "label1";
            label1.Size = new Size(65, 20);
            label1.TabIndex = 0;
            label1.Text = "Pagina 1";
            // 
            // panel3
            // 
            panel3.BackColor = SystemColors.ControlDark;
            panel3.Controls.Add(label3);
            panel3.Location = new Point(643, 397);
            panel3.Name = "panel3";
            panel3.Size = new Size(501, 300);
            panel3.TabIndex = 4;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            label3.Location = new Point(229, 20);
            label3.Name = "label3";
            label3.Size = new Size(65, 20);
            label3.TabIndex = 1;
            label3.Text = "Pagina 3";
            // 
            // panel2
            // 
            panel2.BackColor = SystemColors.Highlight;
            panel2.Controls.Add(label2);
            panel2.Location = new Point(643, 29);
            panel2.Name = "panel2";
            panel2.Size = new Size(501, 300);
            panel2.TabIndex = 5;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            label2.Location = new Point(229, 20);
            label2.Name = "label2";
            label2.Size = new Size(65, 20);
            label2.TabIndex = 0;
            label2.Text = "Pagina 2";
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1190, 741);
            Controls.Add(panel3);
            Controls.Add(panel2);
            Controls.Add(volgendeKnop);
            Controls.Add(vorigeKnop);
            Controls.Add(panel1);
            Name = "Form1";
            Text = "Form1";
            panel1.ResumeLayout(false);
            panel1.PerformLayout();
            panel3.ResumeLayout(false);
            panel3.PerformLayout();
            panel2.ResumeLayout(false);
            panel2.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private Button vorigeKnop;
        private Button volgendeKnop;
        private Panel panel1;
        private Panel panel3;
        private Panel panel2;
        private Label label1;
        private Label label3;
        private Label label2;
    }
}