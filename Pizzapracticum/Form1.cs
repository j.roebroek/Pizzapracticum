namespace Pizzapracticum
{
    public partial class Form1 : Form
    {
        private List<Panel> panelList = new();
        private int panelIndex = 0;
        public Form1()
        {
            InitializeComponent();
            // Zorg ervoor dat het venster altijd een bepaalde grootte heeft
            this.Size = new Size(600, 450);

            // Voeg de panels toe aan de panelList. De panels functioneren zo als pagina's
            AddPanel(panel1);
            AddPanel(panel2);
            AddPanel(panel3);
            panelList[panelIndex].BringToFront();
        }

        // Zet de panels op een vaste grootte en positie en voeg ze toe aan de lijst
        private void AddPanel(Panel panel)
        {
            panel.Location = new Point(40, 30);
            panel.Size = new Size(500, 300);
            panelList.Add(panel);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        // Vorige knop zorgt dat de panelIndex (paginanummer) 1 omlaag gaat
        private void vorigeKnop_Click(object sender, EventArgs e)
        {
            if (panelIndex > 0)
            {
                panelIndex--;
                panelList[panelIndex].BringToFront();
            }
        }

        // Volgende knop zorgt dat de panelIndex (paginanummer) 1 omhoog gaat
        private void volgendeKnop_Click(Object sender, EventArgs e)
        {
            if (panelIndex < panelList.Count - 1)
            {
                panelIndex++;
                panelList[panelIndex].BringToFront();
            }
        }

        // Een lege functie, kopieer deze vooral om zelf iets te doen
        private void voorbeeldFunctie(Object sender, EventArgs e)
        {
        }
    }
}